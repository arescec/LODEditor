# LODEditor

Simple open-source Qt project for mesh decimation.

Uses OpenMesh library: https://www.openmesh.org/svn/

# Features

- Generates simplified meshes for 3 different LODs.
- Side-by-side preview.
- Can import .3ds and .obj files, exports .obj files.

# Requirements

- QtCreator with Qt version 5.8 or newer.
