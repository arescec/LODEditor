#include <QDebug>
#include <QFileInfo>

#include "backend.h"
#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Tools/Decimater/DecimaterT.hh>
#include <OpenMesh/Tools/Decimater/ModQuadricT.hh>

Backend::Backend(QObject *parent) : QObject(parent)
{
}

void Backend::openMesh()
{
    std::string pathStr = path.toStdString();
    OpenMesh::IO::read_mesh(originalMesh, pathStr);
    faceCounts[0] = meshFaceCount(originalMesh);
    qDebug() << "Mesh opened:" << path;
}

void Backend::setWorkingDirectory(QString directory){
    workingDirectory = directory;
}

void Backend::setFilename(QString path)
{
    QFileInfo pInfo(path);

    extension = pInfo.completeSuffix();

    filename = pInfo.fileName();
    filename = filename.replace(".", "");
    filename = filename.replace(extension, "");

    directory = pInfo.path();
    directory = directory.replace("file://", "");

    workingDirectory = workingDirectory.replace("file://", "");

    this->path = path.replace("file://", "");

    qDebug() << extension << "," << filename << "," << directory << "," << this->path;
}

void Backend::saveSimplifiedMesh(int lodNr)
{
    QString savePath = composeLodPath(lodNr);
    savePath = savePath.replace("file://", "");
    std::string pathStr = savePath.toStdString();
    OpenMesh::IO::write_mesh(simplifiedMesh, pathStr);
    qDebug() << "Mesh saved: " << composeLodPath(lodNr);
}

void Backend::decimateMesh(int lodNr)
{
    typedef OpenMesh::Decimater::DecimaterT< Mesh > Decimater;
    typedef OpenMesh::Decimater::ModQuadricT< Mesh >::Handle HModQuadric;

    Mesh mesh = originalMesh;

    Decimater decimater(mesh);
    HModQuadric hModQuadric;
    decimater.add( hModQuadric );

    decimater.module( hModQuadric ).set_max_err(details[lodNr]);
    decimater.initialize();
    decimater.decimate();

    mesh.garbage_collection();

    simplifiedMesh = mesh;

    faceCounts[lodNr + 1] = meshFaceCount(simplifiedMesh);

    qDebug() << "Mesh faces for LOD" << lodNr << ": " << faceCounts[lodNr];

    qDebug() << "LOD" << lodNr << " has been generated";
}

int Backend::meshFaceCount(Mesh mesh){
    unsigned long count = 0;
    for (Mesh::FaceIter f_it=mesh.faces_begin(); f_it!=mesh.faces_end(); ++f_it)
        count++;
    return count;
}

int Backend::getFaceCount(int lodNr){
    return faceCounts[lodNr];
}

void Backend::setLodSuffix(int lodNr, QString suffix)
{
    suffixes[lodNr] = suffix;
}

void Backend::setLodDetail(int lodNr, float detailLevel)
{
    details[lodNr] = detailLevel;
}

QString Backend::composeLodPath(int lodNr)
{
    QString lodPath = workingDirectory.replace("file://", "") + "/" + filename + suffixes[lodNr] + "." + extension;
    return lodPath;
}
