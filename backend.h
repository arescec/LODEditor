#ifndef BACKEND_H
#define BACKEND_H

#include <QObject>
#include <QString>
#include <QQuickView>
#include <QDebug>
#include <QFileDialog>
#include <QString>

#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>

using namespace OpenMesh;

class Backend : public QObject
{
    Q_OBJECT

    public:
        typedef OpenMesh::TriMesh_ArrayKernelT<> Mesh;

        explicit Backend(QObject *parent = 0);

        QString path;
        QString filename;
        QString directory;
        QString extension;
        QString workingDirectory;
        Mesh originalMesh;
        Mesh simplifiedMesh;

        QString suffixes[3] = {};
        float details[3] = {};
        int faceCounts[4] = {0,0,0,0};

        int meshFaceCount(Mesh);

        Q_INVOKABLE void openMesh();
        Q_INVOKABLE void saveSimplifiedMesh(int);
        Q_INVOKABLE void decimateMesh(int);        
        Q_INVOKABLE QString composeLodPath(int);

        Q_INVOKABLE void setLodSuffix(int, QString);
        Q_INVOKABLE void setLodDetail(int, float);
        Q_INVOKABLE void setFilename(QString);
        Q_INVOKABLE void setWorkingDirectory(QString);

        Q_INVOKABLE int getFaceCount(int);
};

#endif // BACKEND_H
