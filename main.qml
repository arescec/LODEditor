import QtQuick.Scene3D 2.0
import QtQuick.Controls 1.2
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.0

import QtQuick 2.2
import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Extras 2.0

Rectangle {
    id: rootRectangle
    //width: parent.width
    //height: parent.height
    width: 1200
    height: 600

    Rectangle {
        id: controlRectangle
        objectName: "controlRectangle"
        width: rootRectangle.width * 0.3
        height: rootRectangle.height
        color: "#e5e5e5"

        property real microMarginH: width * 0.01
        property real smallMarginH: width * 0.05
        property real largeMarginH: width * 0.1
        property real smallMarginV: height * 0.025
        property real largeMarginV: height * 0.5

        Button {
            id: btnOpenFile
            anchors.left: parent.left
            anchors.top: controlRectangle.top
            anchors.leftMargin: controlRectangle.smallMarginH
            anchors.topMargin: 15
            text: "Open File"

            onClicked:
                fileDialog.open()
        }

        // Transform controls
        GroupBox {
            id: transformGroupBox
            title: "View"
            width: parent.width
            anchors.top: btnOpenFile.bottom
            anchors.left: controlRectangle.left
            anchors.right: controlRectangle.right
            anchors.margins: controlRectangle.microMarginH
            anchors.topMargin: 20

            Button {
                id: viewTop
                anchors.left: parent.left
                anchors.leftMargin: controlRectangle.microMarginH
                anchors.topMargin: 15
                width: 80
                height: 34

                Image {
                    anchors.fill: parent
                    source: "images/top_view.png"
                    fillMode: Image.PreserveAspectFit
                    anchors.margins: 5
                }

                onClicked: {
                    settings3D.rotX = 0
                    settings3D.rotY = 270
                }
            }

            Button {
                id: viewSide
                anchors.left: viewTop.right
                anchors.leftMargin: controlRectangle.microMarginH
                anchors.topMargin: 15
                width: 80
                height: 34

                Image {
                    anchors.fill: parent
                    source: "images/side_view.png"
                    fillMode: Image.PreserveAspectFit
                    anchors.margins: 5
                }

                onClicked: {
                    settings3D.rotX = 0
                    settings3D.rotY = 0
                }
            }

            Button {
                id: viewFront
                anchors.left: viewSide.right
                anchors.leftMargin: controlRectangle.microMarginH
                anchors.topMargin: 15
                width: 80
                height: 34

                Image {
                    anchors.fill: parent
                    source: "images/front_view.png"
                    fillMode: Image.PreserveAspectFit
                    anchors.margins: 5
                }

                onClicked: {
                    settings3D.rotX = 90
                    settings3D.rotY = 0
                }
            }

            Button {
                id: viewReset
                anchors.left: viewFront.right
                anchors.leftMargin: controlRectangle.microMarginH
                anchors.topMargin: 15
                width: 80
                height: 34
                smooth: true

                Image {
                    anchors.fill: parent
                    source: "images/reset.png"
                    fillMode: Image.PreserveAspectFit
                    anchors.margins: 5
                }

                onClicked: {
                    settings3D.scale = 1
                    settings3D.rotX = 0
                    settings3D.rotY = 0
                }
            }

        }

        // Suffix controls
        GroupBox {
            id: suffixGroupBox
            title: "Output Suffix"
            width: parent.width
            anchors.top: transformGroupBox.bottom
            anchors.left: controlRectangle.left
            anchors.right: controlRectangle.right
            anchors.margins: controlRectangle.microMarginH

            // LOD 0
            Label {
                id: lod0suffixLabel
                text: "LOD0"
                anchors.left: parent.left
                anchors.leftMargin: controlRectangle.smallMarginH
                anchors.topMargin: 35
            }

            TextField {
                id: lod0suffix
                maximumLength: 8
                text: "_LOD0"
                anchors.left: lod0suffixLabel.right
                anchors.leftMargin: controlRectangle.smallMarginH
                anchors.topMargin: 10

                onEditingFinished: {
                    if(text != lod1suffix.text && text != lod2suffix.text){
                        backend.setLodSuffix(0, text)
                        color = "Black";
                    }
                    else {
                        text = "_LOD0"
                        color = "Red";
                    }
                }

                Component.onCompleted: {
                    backend.setLodSuffix(0, lod0suffix.text)
                }
            }

            // LOD 1
            Label {
                id: lod1suffixLabel
                text: "LOD1"
                anchors.top: lod0suffixLabel.bottom
                anchors.left: parent.left
                anchors.leftMargin: controlRectangle.smallMarginH
                anchors.topMargin: 10
            }

            TextField {
                id: lod1suffix
                maximumLength: 8
                text: "_LOD1"
                anchors.top: lod0suffixLabel.bottom
                anchors.left: lod1suffixLabel.right
                anchors.leftMargin: controlRectangle.smallMarginH
                anchors.topMargin: 10

                onEditingFinished: {
                    if(text != lod0suffix.text && text != lod2suffix.text){
                        backend.setLodSuffix(1, text)
                        color = "Black";
                    }
                    else {
                        text = "_LOD1"
                        color = "Red";
                    }
                }

                Component.onCompleted: backend.setLodSuffix(1, lod1suffix.text);
            }

            // LOD 2
            Label {
                id: lod2suffixLabel
                text: "LOD2"
                anchors.top: lod1suffixLabel.bottom
                anchors.left: parent.left
                anchors.leftMargin: controlRectangle.smallMarginH
                anchors.topMargin: 10
            }

            TextField {
                id: lod2suffix
                maximumLength: 8
                text: "_LOD2"
                anchors.top: lod1suffixLabel.bottom
                anchors.left: lod2suffixLabel.right
                anchors.leftMargin: controlRectangle.smallMarginH
                anchors.topMargin: 10

                onEditingFinished: {
                    if(text != lod0suffix.text && text != lod1suffix.text){
                        backend.setLodSuffix(2, text)
                        color = "Black";
                    }
                    else {
                        text = "_LOD2"
                        color = "Red";
                    }
                }
                Component.onCompleted: backend.setLodSuffix(2, lod2suffix.text);
            }

        }

        GroupBox {
            id: detailGroupBox
            title: "Error Level"
            width: parent.width
            anchors.top: suffixGroupBox.bottom
            anchors.left: controlRectangle.left
            anchors.right: controlRectangle.right
            anchors.margins: controlRectangle.microMarginH

            // LOD0 Detail
            Label {
                id: lod0detailLabel
                text: "LOD0"
                anchors.left: parent.left
                anchors.leftMargin: controlRectangle.smallMarginH
            }

            Slider {
                id: lod0detailSlider
                anchors.left: lod0detailLabel.right
                anchors.right: parent.right
                anchors.leftMargin: controlRectangle.smallMarginH
                anchors.rightMargin: controlRectangle.smallMarginH
                anchors.topMargin: 7
                value: 0.001
                maximumValue: 0.02
                minimumValue: 0
                stepSize: 0.0001

                onValueChanged:
                {
                    backend.setLodDetail(0, value)

                    if (value != meshSceneLOD0.previewErrorLevel)
                        meshSceneLOD0.showWarning = true
                    else
                        meshSceneLOD0.showWarning = false
                }
                Component.onCompleted:
                {
                    backend.setLodDetail(0, value)

                    meshSceneLOD0.previewErrorLevel = value
                    meshSceneLOD0.showWarning = false
                }
            }

            TextInput {
                id: lod0sliderText
                text: lod0detailSlider.value
                maximumLength: 5
                width: 40
                anchors.top: lod0detailSlider.bottom
                anchors.left: lod0detailSlider.left
                anchors.leftMargin: (lod0detailSlider.width - 25) * (lod0detailSlider.value * (1/lod0detailSlider.maximumValue))
            }

            // LOD1 Detail
            Label {
                id: lod1detailLabel
                text: "LOD1"
                anchors.top: lod0detailLabel.bottom
                anchors.left: parent.left
                anchors.leftMargin: controlRectangle.smallMarginH
                anchors.topMargin: 23
            }

            Slider {
                id: lod1detailSlider
                anchors.top: lod0detailLabel.bottom
                anchors.left: lod1detailLabel.right
                anchors.right: parent.right
                anchors.leftMargin: controlRectangle.smallMarginH
                anchors.rightMargin: controlRectangle.smallMarginH
                anchors.topMargin: 20
                value: 0.01
                maximumValue: 0.04
                minimumValue: 0
                stepSize: 0.0001

                onValueChanged:
                {
                    backend.setLodDetail(1, value)

                    if (value != meshSceneLOD1.previewErrorLevel)
                        meshSceneLOD1.showWarning = true
                    else
                        meshSceneLOD1.showWarning = false
                }
                Component.onCompleted:
                {
                    backend.setLodDetail(1, value)

                    meshSceneLOD1.previewErrorLevel = value
                    meshSceneLOD1.showWarning = false
                }
            }

            TextInput {
                id: lod1sliderText
                text: lod1detailSlider.value
                maximumLength: 5
                width: 40
                anchors.top: lod1detailSlider.bottom
                anchors.left: lod1detailSlider.left
                anchors.leftMargin: (lod1detailSlider.width - 25) * (lod1detailSlider.value * (1/lod1detailSlider.maximumValue))
            }

            // LOD2 Detail
            Label {
                id: lod2detailLabel
                text: "LOD2"
                anchors.top: lod1detailLabel.bottom
                anchors.left: parent.left
                anchors.leftMargin: controlRectangle.smallMarginH
                anchors.topMargin: 23
            }

            Slider {
                id: lod2detailSlider
                anchors.top: lod1detailLabel.bottom
                anchors.left: lod2detailLabel.right
                anchors.right: parent.right
                anchors.leftMargin: controlRectangle.smallMarginH
                anchors.rightMargin: controlRectangle.smallMarginH
                anchors.topMargin: 20
                value: 0.1
                maximumValue: 0.1
                minimumValue: 0
                stepSize: 0.0001

                onValueChanged:
                {
                    backend.setLodDetail(2, value)

                    if (value != meshSceneLOD2.previewErrorLevel)
                        meshSceneLOD2.showWarning = true
                    else
                        meshSceneLOD2.showWarning = false
                }
                Component.onCompleted:
                {
                    backend.setLodDetail(2, value)

                    meshSceneLOD2.previewErrorLevel = value
                    meshSceneLOD2.showWarning = false
                }
            }

            TextInput {
                id: lod2sliderText
                text: lod2detailSlider.value
                maximumLength: 5
                width: 40
                anchors.top: lod2detailSlider.bottom
                anchors.left: lod2detailSlider.left
                anchors.leftMargin: (lod2detailSlider.width - 25) * (lod2detailSlider.value * (1/lod2detailSlider.maximumValue))
            }
        }

        GroupBox {
            id: savePathGroupBox
            title: "Output Directory"
            width: parent.width
            anchors.top: detailGroupBox.bottom
            anchors.left: controlRectangle.left
            anchors.right: controlRectangle.right
            anchors.margins: controlRectangle.microMarginH

            FileDialog {
               id: saveFileDialog
               visible: false
               selectFolder: true
               selectMultiple: false

               property string exportPath : shortcuts.home

               onAccepted: {
                   exportPath = saveFileDialog.fileUrl
                   backend.setWorkingDirectory(exportPath)
               }

               Component.onCompleted: {
                   backend.setWorkingDirectory(shortcuts.home)
               }
            }

            Label {
                id: savePathText
                text: saveFileDialog.exportPath
                width: controlRectangle.width * 0.8
            }

            Button {
                id: savePathButton
                anchors.top: savePathText.bottom
                width: controlRectangle.width * 0.2
                text: "Change"

                onClicked: {
                    saveFileDialog.visible = true
                }
            }
        }

        Button {
            id: btnDecimate
            anchors.right: controlRectangle.right
            anchors.bottom: controlRectangle.bottom
            anchors.margins: controlRectangle.microMarginH
            anchors.leftMargin: controlRectangle.microMarginH
            text: "Generate"
            enabled: false

            onClicked:
            {
                if(meshSceneLOD0.showWarning || meshSceneLOD0.meshSource == ""){
                    backend.decimateMesh(0)
                    backend.saveSimplifiedMesh(0)
                    meshSceneLOD0.meshSource = ""
                    meshSceneLOD0.meshSource = "file://" + backend.composeLodPath(0)
                    meshSceneLOD0.previewErrorLevel = lod0detailSlider.value
                    meshSceneLOD0.showWarning = false
                    meshSceneLOD0.sceneTitle = "LOD0 (" + backend.getFaceCount(1) + " faces)"
                    console.log("Loading mesh preview from file " + meshSceneLOD0.meshSource)
                }

                if(meshSceneLOD1.showWarning || meshSceneLOD1.meshSource == ""){
                    backend.decimateMesh(1)
                    backend.saveSimplifiedMesh(1)
                    meshSceneLOD1.meshSource = ""
                    meshSceneLOD1.meshSource = "file://" + backend.composeLodPath(1)
                    meshSceneLOD1.previewErrorLevel = lod1detailSlider.value
                    meshSceneLOD1.showWarning = false
                    meshSceneLOD1.sceneTitle = "LOD1 (" + backend.getFaceCount(2) + " faces)"
                    console.log("Loading mesh preview from file " + meshSceneLOD1.meshSource)
                }

                if(meshSceneLOD2.showWarning || meshSceneLOD2.meshSource == ""){
                    backend.decimateMesh(2)
                    backend.saveSimplifiedMesh(2)
                    meshSceneLOD2.meshSource = ""
                    meshSceneLOD2.meshSource = "file://" + backend.composeLodPath(2)
                    meshSceneLOD2.previewErrorLevel = lod2detailSlider.value
                    meshSceneLOD2.showWarning = false
                    meshSceneLOD2.sceneTitle = "LOD2 (" + backend.getFaceCount(3) + " faces)"
                    console.log("Loading mesh preview from file " + meshSceneLOD2.meshSource)
                }
            }
        }
    }

    Item {
        id: settings3D

        property real scale : 1
        property real rotX : 0
        property real rotY : 0
    }

    // Original Preview
    MeshPreview {
        id: meshSceneOriginal
        anchors.left: controlRectangle.right
        anchors.top: parent.top
        sceneTitle: "Original"
    }

    // LOD0 Preview
    MeshPreview {
        id: meshSceneLOD0
        anchors.right: parent.right
        anchors.top: parent.top
        sceneTitle: "LOD0"
    }

    // LOD1 Preview
    MeshPreview {
        id: meshSceneLOD1
        anchors.left: controlRectangle.right
        anchors.bottom: parent.bottom
        sceneTitle: "LOD1"
    }

    // LOD2 Preview
    MeshPreview {
        id: meshSceneLOD2
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        sceneTitle: "LOD2"
    }

    // Open File
    FileDialog {
        id: fileDialog
        title: "Select a 3D file."
        visible: false
        selectFolder: false
        selectMultiple: false
        folder: shortcuts.home
        nameFilters: [ "3D Files (*.obj *.3ds)"]
        modality: Qt.NonModal

        property string filename
        property bool fileLoaded : false;
        signal meshLoaded

        onAccepted: {
            filename = fileDialog.fileUrl

            backend.setFilename(filename)
            backend.openMesh(filename)
            fileLoaded = true

            meshSceneOriginal.meshSource = fileDialog.filename
            btnDecimate.enabled = true

            meshSceneOriginal.sceneTitle = "Original (" + backend.getFaceCount(0) + " faces)"
        }

        onRejected: {
            fileLoaded = false
        }
    }

    MouseArea {
        id: mouseArea
        anchors.left: controlRectangle.right
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        acceptedButtons: Qt.LeftButton | Qt.RightButton

        property real dx
        property real dy
        property real oldX
        property real oldY
        property bool lmb : false
        property bool rmb : false

        onClicked: {
            oldX = mouse.x
            oldY = mouse.y
        }

        onWheel: {
            settings3D.scale += wheel.angleDelta.y / 360
            if(settings3D.scale < 0.1) settings3D.scale = 0.1
            console.log("Scale: " + settings3D.scale)
        }

        onPositionChanged: {
            dx = mouse.x - oldX
            dy = mouse.y - oldY

            oldX = mouse.x
            oldY = mouse.y

            settings3D.rotX += dx
            settings3D.rotY += dy
        }
    }
}
