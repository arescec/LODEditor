import QtQuick.Scene3D 2.0
import QtQuick.Controls 1.2
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.0

import QtQuick 2.7
import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Extras 2.0

Rectangle {
   id: root
   width: rootRectangle.width * 0.35
   height: rootRectangle.height * 0.5
   color: "white"
   border.width: 1
   border.color: "lightGrey"

   property string meshSource : ""
   property string sceneTitle
   property bool showWarning : false
   property real previewErrorLevel

   Scene3D {
       id: rootScene
       anchors.fill: parent
       cameraAspectRatioMode: Scene3D.AutomaticAspectRatio

       Entity {
           id: rootEntity

           Camera {
               id: camera
               projectionType: CameraLens.PerspectiveProjection
               fieldOfView: 30
               nearPlane : 0.1
               farPlane : 1000.0
               position: Qt.vector3d( 40.0, 0.0, 0.0 )
               upVector: Qt.vector3d( 0.0, 1.0, 0.0 )
               viewCenter: Qt.vector3d( 0.0, 0.0, 0.0 )
           }

           FirstPersonCameraController { camera: camera }

           components: [
               RenderSettings {
                   activeFrameGraph: ForwardRenderer {
                       id: renderer
                       camera: camera
                       clearColor: "transparent"
                   }
               },
               InputSettings { }
           ]

           PhongMaterial {
               id: material
               ambient: Qt.rgba( 0.2, 0.2, 0.2, 1.0 )
               diffuse: Qt.rgba( 0.8, 0.8, 0.8, 1.0 )
           }

           Transform {
               id: targetTransform

               matrix: {
                   var m = Qt.matrix4x4();
                   m.rotate(settings3D.rotX, Qt.vector3d(0, 1, 0))
                   m.rotate(settings3D.rotY, Qt.vector3d(0, 0, 1))
                   m.scale(settings3D.scale)
                   return m;
               }
           }

           Mesh {
               id: mesh
               source: root.meshSource
           }

           Entity {
               id: entity3D
               components: [ targetTransform, mesh, material ]
           }
       }
   }

   Rectangle {
       id: titleRectangle
       height: 30
       width: sceneLabel.width + 20
       anchors.bottom: parent.bottom
       anchors.right: parent.right
       anchors.margins: 10
       border.color: "#cccccc"
       border.width: 1
       color: "#eeeeee"
       radius: 5

       Label {
            id: sceneLabel
            text: root.sceneTitle
            anchors.margins: 5
            anchors.leftMargin: 10
            anchors.left: parent.left
            anchors.top: parent.top
       }
   }

   Rectangle {
       id: warningRectangle
       width: 140
       height: 30
       anchors.bottom: parent.bottom
       anchors.left: parent.left
       anchors.margins: 10
       border.color: "#cccccc"
       border.width: 1
       color: "Red"
       radius: 5
       visible: root.showWarning

       Label {
            id: warningLabel
            text: "Preview Outdated"
            color: "White"
            anchors.margins: 5
            anchors.leftMargin: 10
            anchors.left: parent.left
            anchors.top: parent.top
       }
   }
}

