#include <QGuiApplication>
#include <QQuickView>
#include <QDebug>
#include <QQmlContext>
#include <QQuickItem>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "backend.h"

void openFile()
{
    qDebug() << "Hello world";
}

int main(int argc, char **argv)
{
    QGuiApplication app(argc, argv);

    QQuickView view;
    Backend backend;
    view.rootContext()->setContextProperty("backend", &backend);
    view.setSource(QUrl("qrc:/main.qml"));
    view.show();

    return app.exec();
}


