QT += qml quick 3dinput widgets 3dcore 3drender 3dquick 3dquickextras

SOURCES += \
    backend.cpp \
    main.cpp

OTHER_FILES += \
    main.qml \
    MeshPreview.qml

RESOURCES += \
    scene3d.qrc

HEADERS += \
    backend.h

LIBS += -L$$PWD/libs -lOpenMeshCore \
        -L$$PWD/libs -lOpenMeshTools

INCLUDEPATH += \
    $$PWD/libs

DISTFILES += \
    MeshPreview.qml
